# 7 Days to Die bot manager

```
usage: 7dtd-botman [-h] [--password PASSWORD]
                   [--log-level {debug,info,warn,error}]
                   host port

7 Days to Die monitor and bot

positional arguments:
  host                  Server host name or ip address
  port                  Port of server

optional arguments:
  -h, --help            show this help message and exit
  --password PASSWORD   Telnet password for server
  --log-level {debug,info,warn,error}
                        Logger level
```
