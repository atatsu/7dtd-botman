import asyncio
from unittest import TestCase
from unittest.mock import Mock, patch

from testing import AsyncMock

from botman.bots import AuthBot
from botman.exceptions import AuthenticationError


class AuthBotTests(TestCase):

    def setUp(self):
        self.loop = asyncio.get_event_loop()

        self._future = Mock()

        self.password = 'secret'
        self.bot = AuthBot(self.password, self._future)

        self.auth_prompt = [
            b'garbage',
            b'Please enter password',
            b'garbage',
        ]
        self.auth_fail = [
            b'garbage',
            b'Password incorrect',
            b'garbage',
        ]
        self.auth_success = [
            b'garbage',
            b'Logon successful',
            b'garbage',
        ]

    @patch('botman.bots.AuthBot.queue_write', new_callable=AsyncMock)
    def test_queue_password(self, _queue_write):
        """when prompted queue password for sending"""
        self.loop.run_until_complete(self.bot.work(self.auth_prompt))
        self.assertTrue(self.bot.authenticating)
        _queue_write.assert_called_once_with(self.password)

    def test_auth_fail(self):
        """
        on auth fail set future exception AuthenticationError
        """
        self.bot.authenticating = True
        self.loop.run_until_complete(self.bot.work(self.auth_fail))
        self.assertFalse(self.bot.is_active, 'bot should be deactive')
        self._future.set_exception.assert_called_once_with(AuthenticationError)

    def test_auth_success(self):
        """
        on auth success the future should be marked as done
        """
        self.bot.authenticating = True
        self.loop.run_until_complete(self.bot.work(self.auth_success))
        self.assertFalse(self.bot.is_active, 'bot should be deactive')
        self._future.set_result.assert_called_once_with(None)

