import asyncio
from unittest import TestCase

from botman.bots import Bot


class EmptyBot(Bot):

    async def work(self, data):
        pass


class BotTestCase(TestCase):

    def setUp(self):
        self.bot = EmptyBot()
        self.loop = asyncio.get_event_loop()

        self.bot.loop = self.loop


class BotSleepTests(BotTestCase):

    def test_sleep(self):
        """sleep for specified amount of time"""
        self.assertTrue(self.bot.is_awake)
        future = self.bot.sleep(0)
        self.assertFalse(self.bot.is_awake, 'bot should have been sleeping')
        self.loop.run_until_complete(future)
        self.assertTrue(self.bot.is_awake, 'bot should have been awake')

    def test_sleep_multi(self):
        """multiple calls to sleep should cancel original"""
        first_task = self.bot.sleep(0)
        second_task = self.bot.sleep(0)
        #self.loop.run_until_complete(first_task)
        self.loop.run_until_complete(second_task)

        self.assertTrue(first_task.cancelled(), 'task was not cancelled')
        self.assertIsNone(self.bot.task)


class BotDeactivateTests(BotTestCase):

    def test_no_longer_active(self):
        """bot should no longer be active"""
        self.loop.run_until_complete(self.bot.deactivate())
        self.assertFalse(self.bot.is_active, 'bot should not be active')

    def test_cancel_sleep_task(self):
        """cancel the sleep task if it is present"""
        task = self.bot.sleep(10)
        self.loop.run_until_complete(self.bot.deactivate())
        self.assertTrue(task.cancelled(), 'sleep task not cancelled')
