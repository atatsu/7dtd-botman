import asyncio
from unittest import TestCase
from unittest.mock import patch, Mock

from testing import AsyncMock

from botman.bots import FeralCountdownBot
from botman.commands import GET_TIME


class ReTest(TestCase):

    def setUp(self):
        self.re = FeralCountdownBot.re_day_time

    def test_matches(self):
        self.assertIsNotNone(
            self.re.search(b'Day 203, 20:02')
        )

    def test_group(self):
        match = self.re.search(b'Day 203, 20:02 ')
        self.assertEquals(b'203', match.group('day'))
        self.assertEquals(b'20', match.group('hour'))
        self.assertEquals(b'02', match.group('minute'))


class FeralCountdownBotTests(TestCase):

    def setUp(self):
        self.loop = asyncio.get_event_loop()
        self.bot = FeralCountdownBot()

class NonParsingState(FeralCountdownBotTests):

    @patch('botman.bots.FeralCountdownBot.queue_write', new_callable=AsyncMock)
    def setUp(self, _queue_write):
        super().setUp()
        self._queue_write = _queue_write

        self.parsing_state = self.bot.parsing

        self.loop.run_until_complete(self.bot.work([]))


    def test_initial_non_parsing(self):
        """not in parsing state when first setup"""
        self.assertFalse(self.parsing_state)

    def test_queue_command(self):
        """if not parsing bot should send command to get current time"""
        self._queue_write.assert_called_once_with(GET_TIME)

    def test_parsing_after_command(self):
        """after command queued should be parsing"""
        self.assertTrue(self.bot.parsing)

class ParsingState(FeralCountdownBotTests):

    data = []

    @patch('botman.bots.FeralCountdownBot.sleep')
    @patch('botman.bots.FeralCountdownBot.queue_write', new_callable=AsyncMock)
    def setUp(self, _queue_write, _sleep):
        super().setUp()

        self._queue_write = _queue_write
        self._sleep = _sleep

        self.loop.run_until_complete(self.bot.work([]))
        self._queue_write.reset_mock()
        self.loop.run_until_complete(self.bot.work(self.data))

class DayOfTests(ParsingState):

    data = [
        b'garbage',
        b'Day 266, 14:17',
        b'garbage',
    ]

    def test_msg_queued(self):
        """queue message for write that conveys time remaining"""
        expected = ('say "Feral horde will spawn in 0 days, 7 hours, '
                    'and 43 minutes (266 22:00)!"')
        self._queue_write.assert_called_once_with(expected)

    def test_no_longer_parsing(self):
        """after notifying should no longer be in parsing state"""
        self.assertFalse(self.bot.parsing)

    def test_sleep(self):
        """after notifying sleep for 5 minutes (300 seconds)"""
        self._sleep.assert_called_once_with(300)

class DayPriorTests(ParsingState):

    data = [
        b'garbage',
        b'Day 265, 08:00',
        b'garbage',
    ]

    def test_msg_queued(self):
        """queue message for write that conveys time remaining"""
        expected = 'say "Feral horde will spawn in 1 day (266 22:00)!"'
        self._queue_write.assert_called_once_with(expected)

    def test_no_longer_parsing(self):
        """after notifying should no longer be in parsing state"""
        self.assertFalse(self.bot.parsing)

    def test_sleep(self):
        """after notifying sleep for 10 minutes (600 seconds)"""
        self._sleep.assert_called_once_with(600)


class MoreThanDayTests(ParsingState):

    data = [
        b'garbage',
        b'Day 264, 21:29',
        b'garbage',
    ]

    def test_msg_queued(self):
        """queue message for write that conveys time remaining"""
        expected = 'say "Feral horde will spawn in 2 days (266 22:00)."'
        self._queue_write.assert_called_once_with(expected)

    def test_no_longer_parsing(self):
        """after notifying should no longer be in parsing state"""
        self.assertFalse(self.bot.parsing)

    def test_sleep(self):
        """after notifying sleep for 15 minutes (900 seconds)"""
        self._sleep.assert_called_once_with(900)


class AlreadySpawned(ParsingState):

    data = [
        b'garbage',
        b'Day 266, 23:00',
        b'garbage',
    ]

    def test_no_notification(self):
        """no notification should be queued"""
        self._queue_write.assert_not_called()

    def test_no_longer_parsing(self):
        """after notifying should no longer be in parsing state"""
        self.assertFalse(self.bot.parsing)

    def test_sleep(self):
        """no notification needed, sleep for 15 minutes (900) seconds)"""
        self._sleep.assert_called_once_with(900)
