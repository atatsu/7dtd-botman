import asyncio
from asyncio import Queue
from contextlib import suppress
from functools import partial
from unittest import TestCase, skip
from unittest.mock import Mock, call, patch

from testing import AsyncMock

from botman import BotManager, ReadCycleManager, WriteCycleManager, SleepCycleManager
from botman.bots import Bot


class EmptyBot(Bot):

    async def work(self, data):
        pass


class ReadCycleManagerTests(TestCase):

    def setUp(self):
        self._telnet = Mock()

        self.telnet_return_values = []
        def telnet_side_effect():
            return self.telnet_return_values.pop(0)
        self._telnet.read_very_eager.side_effect = telnet_side_effect

        self.loop = asyncio.get_event_loop()
        self.reader = ReadCycleManager(self._telnet, loop=self.loop)

    def test_telnet_polled(self):
        """poll telnet connection with every invocation"""
        self.telnet_return_values.append(b'')
        self.loop.run_until_complete(self.reader.work())
        self._telnet.read_very_eager.assert_called_once_with()

    def test_data_added_to_queue(self):
        """any read data should be added to the data queue"""
        return_value = b'stuff\r\n'
        self.telnet_return_values.append(return_value)

        self.loop.run_until_complete(self.reader.work())

        data = self.loop.run_until_complete(
            self.reader.all_data()
        )
        self.assertEquals((return_value.strip(b'\r\n'),), data)

    def test_buffer_incomplete_data(self):
        """incomplete data should be buffered"""
        return_values = [
            b'incomplete ',
            b'stuff\r\n',
        ]
        self.telnet_return_values.extend(return_values)

        self.loop.run_until_complete(self.reader.work())
        self.loop.run_until_complete(self.reader.work())

        expected = b''.join(return_values).strip(b'\r\n')
        actual = self.loop.run_until_complete(
            self.reader.all_data()
        )
        self.assertEquals((expected,), actual)

    def test_discard_blank_data(self):
        """throw away blank data"""
        return_values = [
            b'\r\n',
            b'\r\n',
        ]
        self.telnet_return_values.extend(return_values)

        self.loop.run_until_complete(self.reader.work())
        self.loop.run_until_complete(self.reader.work())

        self.assertFalse(self.reader.has_data)

    def test_discard_no_data(self):
        """throw away empty data"""
        return_values = [
            b'',
            b'',
        ]
        self.telnet_return_values.extend(return_values)

        self.loop.run_until_complete(self.reader.work())
        self.loop.run_until_complete(self.reader.work())

        self.assertFalse(self.reader.has_data)

    def test_queue_data_by_line(self):
        """received data should be queued by individual lines"""
        return_values = [
            b'my first line\r\nmy second line\r\n',
            b'my third ',
            b'line\r\n',
        ]

        self.telnet_return_values.extend(return_values)

        self.loop.run_until_complete(self.reader.work())
        self.loop.run_until_complete(self.reader.work())
        self.loop.run_until_complete(self.reader.work())

        expected = (b'my first line',
                    b'my second line',
                    b'my third line'
                    )
        actual = self.loop.run_until_complete(
            self.reader.all_data()
        )

        self.assertEquals(expected, actual)

    def test_no_queue_data(self):
        """if not in queue data mode discard read data"""
        return_values = [
            b'my first line\r\nmy second line\r\n',
            b'my third ',
            b'line\r\n',
        ]
        self.telnet_return_values.extend(return_values)

        self.reader.storage_mode = False

        self.loop.run_until_complete(self.reader.work())
        self.loop.run_until_complete(self.reader.work())
        self.loop.run_until_complete(self.reader.work())

        actual = self.loop.run_until_complete(
            self.reader.all_data()
        )
        self.assertEquals((), actual)


class WriteCycleManagerTests(TestCase):

    def setUp(self):
        self._telnet = Mock()
        self.loop = asyncio.get_event_loop()
        self.writer = WriteCycleManager(self._telnet, loop=self.loop)

        self.writer.data_q.put_nowait('one')
        self.writer.data_q.put_nowait('two')
        self.writer.data_q.put_nowait('three')

    def test_write_cycle(self):
        """everything in queue should be sent via telnet connection"""
        self.loop.run_until_complete(self.writer.work())
        expected = [
            call('one\n'.encode('ascii')),
            call('two\n'.encode('ascii')),
            call('three\n'.encode('ascii'))
        ]
        self._telnet.write.assert_has_calls(expected)


class SleepCycleManagerTests(TestCase):

    def setUp(self):
        self.loop = asyncio.get_event_loop()

        self.awake_queue = Queue(maxsize=1)
        self.asleep_queue = Queue(maxsize=1)

        self.manager = SleepCycleManager(
            asleep_queue=self.asleep_queue, awake_queue=self.awake_queue, loop=self.loop
        )

        self.bot = EmptyBot()
        self.loop.call_later(2, self.loop.stop)

    def test_move_awake_bots(self):
        """bots residing in the asleep queue that have awoken should be moved"""
        self.asleep_queue.put_nowait(self.bot)

        self.loop.run_until_complete(self.manager.work())

        self.assertTrue(self.awake_queue.full(), 'bot not moved to awake queue')
        self.assertTrue(self.asleep_queue.empty(), 'bot should not be in asleep queue')

    def test_requeue_still_sleeping(self):
        """bots checked but still asleep should be placed back on asleep queue"""
        self.bot.sleep(10)
        self.asleep_queue.put_nowait(self.bot)

        self.loop.run_until_complete(self.manager.work())

        self.assertTrue(self.awake_queue.empty(), 'bot should not have been moved to awake q')
        self.assertTrue(self.asleep_queue.full(), 'bot should have been requeued on asleep q')

    def test_move_asleep_bots(self):
        """bots residing in the awake q that have fallen asleep should be moved"""
        self.bot.sleep(10)
        self.awake_queue.put_nowait(self.bot)

        self.loop.run_until_complete(self.manager.work())

        self.assertTrue(self.awake_queue.empty(), 'bot should not have remained on awake q')
        self.assertTrue(self.asleep_queue.full(), 'bot should have been requeued on asleep q')

    def test_requeue_still_awake(self):
        """bots checked but still awake should be placed back on awake q"""
        self.awake_queue.put_nowait(self.bot)

        self.loop.run_until_complete(self.manager.work())

        self.assertTrue(self.awake_queue.full(), 'bot should have been requeued with awake q')
        self.assertTrue(self.asleep_queue.empty(), 'asleep q should be empty')

    def test_drop_deactive(self):
        """deactive bots should be dropped from both queues"""
        self.awake_queue.put_nowait(self.bot)
        self.asleep_queue.put_nowait(self.bot)

        self.loop.run_until_complete(self.bot.deactivate())
        self.loop.run_until_complete(self.manager.work())

        self.assertTrue(self.awake_queue.empty(), 'awake q should be empty')
        self.assertTrue(self.asleep_queue.empty(), 'asleep q should be empty')


class SleepCyclePermaSleepTests(TestCase):

    def setUp(self):
        self.loop = asyncio.get_event_loop()

        self.awake_q = Queue(maxsize=1)
        self.asleep_q = Queue(maxsize=1)

        self.manager = SleepCycleManager(
            asleep_queue=self.asleep_q, awake_queue=self.awake_q, loop=self.loop
        )

        self.bot = EmptyBot()
        # ensures the loop will stop in case our coro doesn't end like it should
        self.loop.call_later(2, self.loop.stop)

    def test_all_sleeping_bots_phase_shifted(self):
        """move all sleeping bots to phase shifted  q"""
        self.asleep_q.put_nowait(self.bot)
        self.manager.phase_shift()

        self.loop.run_until_complete(self.manager.work())

        self.assertTrue(self.asleep_q.empty(), 'bot not phase shifted')
        self.assertEquals(1, self.manager.phase_shift_q.qsize())

    def test_all_awake_bots_phase_shifted(self):
        """move all waking bots to phase shifted q"""
        self.awake_q.put_nowait(self.bot)
        self.manager.phase_shift()

        self.loop.run_until_complete(self.manager.work())

        self.assertTrue(self.asleep_q.empty(), 'bot not phase shifted')
        self.assertEquals(1, self.manager.phase_shift_q.qsize())

    def test_exclude_bots_ignored(self):
        """bots passed in as excludes should not be phase shifted"""
        bot2 = EmptyBot()
        bot2.sleep(10)
        self.awake_q.put_nowait(self.bot)
        self.asleep_q.put_nowait(bot2)
        self.manager.phase_shift(EmptyBot)

        self.loop.run_until_complete(self.manager.work())
        self.assertFalse(self.asleep_q.empty(), 'bot should not have been phase shifted')
        self.assertFalse(self.awake_q.empty(), 'bot should not have been phase shifted')

    def test_multiple_phase_shift_noop(self):
        """multiple calls to phase shift  should have no affect until stopped"""
        self.manager.phase_shift(EmptyBot)
        self.assertEquals((EmptyBot,), self.manager.phase_shift_excludes)
        self.manager.phase_shift()
        self.assertEquals((EmptyBot,), self.manager.phase_shift_excludes)

    def test_restore_bots_after_phase_shift(self):
        """when phase shift ends bots need to go back to the appropriate q"""
        sleeper_bot = EmptyBot()
        sleeper_bot.sleep(10)

        self.manager.phase_shift_q.put_nowait(self.bot)
        self.manager.phase_shift_q.put_nowait(sleeper_bot)

        # this isn't necessary as the manager starts out in
        # a non-phase shift state but it's here for clarity
        self.manager.end_phase_shift()
        self.loop.run_until_complete(self.manager.work())

        self.assertTrue(self.manager.phase_shift_q.empty())
        self.assertFalse(self.awake_q.empty())
        self.assertFalse(self.asleep_q.empty())

    def test_cleanup_on_stop(self):
        """deactivate phase shifted bots on stop"""
        self.manager.phase_shift_q.put_nowait(self.bot)
        self.loop.run_until_complete(self.manager.cleanup())

        self.assertFalse(self.bot.is_active)


class BotManagerStartTests(TestCase):

    @patch('botman.WriteCycleManager')
    @patch('botman.ReadCycleManager')
    @patch('botman.Telnet')
    def setUp(self, _telnet, _reader, _writer):
        _telnet.return_value = _telnet
        self._telnet = _telnet
        _reader.return_value = _reader
        _reader.start = AsyncMock()
        self._reader = _reader
        _writer.return_value = _writer
        _writer.start = AsyncMock()
        self._writer = _writer

        self.loop = asyncio.get_event_loop()
        self.botman = BotManager('localhost', 8081, 'secret', loop=self.loop)
        self.loop.run_until_complete(self.botman.start())

    def test_connect_telnet(self):
        """establish a telnet connection"""
        self._telnet.assert_called_once_with(host='localhost', port=8081)

    def test_reader_setup(self):
        """setup the reader"""
        self._reader.assert_called_once_with(self._telnet, loop=self.loop)

    def test_writer_setup(self):
        """setup the writer"""
        self._writer.assert_called_once_with(self._telnet, loop=self.loop)

    def test_reader_started(self):
        """start the reader"""
        self._reader.start.assert_called_once_with()

    def test_writer_started(self):
        """start the writer"""
        self._writer.start.assert_called_once_with()


class BotManagerStopCleanup(TestCase):

    @patch('botman.WriteCycleManager')
    @patch('botman.ReadCycleManager')
    @patch('botman.Telnet')
    def setUp(self, _telnet, _reader, _writer):
        _telnet.return_value = _telnet
        _reader.return_value = _reader
        _reader.start = AsyncMock()
        _reader.stop = AsyncMock()
        _writer.return_value = _writer
        _writer.start = AsyncMock()
        _writer.stop = AsyncMock()

        self._telnet = _telnet
        self._reader = _reader
        self._writer = _writer

        self.loop = asyncio.get_event_loop()
        self.botman = BotManager('localhost', 8081, 'secret', loop=self.loop)
        self.loop.run_until_complete(self.botman.start())
        self.loop.run_until_complete(self.botman.cleanup())

    def test_reader_stopped(self):
        """stop the reader"""
        self._reader.stop.assert_called_once_with()

    def test_writer_stopped(self):
        """stop the writer"""
        self._writer.stop.assert_called_once_with()

    def test_stop_periodic(self):
        """stop periodic execution"""
        #self._loop.run_until_complete.assert_called_once_with(self._stop())
        #self._stop.assert_called_once_with()


class BotManagerCycleTests(TestCase):

    @patch('botman.WriteCycleManager', new_callable=AsyncMock)
    @patch('botman.ReadCycleManager', new_callable=AsyncMock)
    @patch('botman.Telnet')
    def setUp(self, _telnet, _reader, _writer):
        self._telnet = _telnet
        self._reader = _reader
        self._writer = _writer

        self.loop = asyncio.get_event_loop()
        self.botman = BotManager('localhost', 8081, 'secret', loop=self.loop)
        self.botman.start()
