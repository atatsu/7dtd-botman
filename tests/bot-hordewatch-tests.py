import asyncio
from unittest import TestCase
from unittest.mock import patch, Mock

from testing import AsyncMock

from botman.bots import HordeWatchBot


class ReTest(TestCase):

    def setUp(self):
        self.re = HordeWatchBot.re_horde_spawn
        self.search_text = (b'2016-08-06T11:58:28 75470.709 INF AIDirector: Spawning '
                            b"wandering horde moving towards player '[type=EntityPlayer, "
                            b"name=Calmconfused, id=9391]'")

    def test_matches(self):
        self.assertIsNotNone(self.re.search(self.search_text))

    def test_groups(self):
        match = self.re.search(self.search_text)
        self.assertEquals(b'Calmconfused', match.group('player_name'))


class HordeWatchBotTests(TestCase):

    data = [
        b'garbage',
        (b'2016-08-06T11:58:28 75470.709 INF AIDirector: Spawning '
         b"wandering horde moving towards player '[type=EntityPlayer, "
         b"name=Calmconfused, id=9391]'"),
        b'garbage',
    ]

    @patch('botman.bots.HordeWatchBot.sleep')
    @patch('botman.bots.HordeWatchBot.queue_write', new_callable=AsyncMock)
    def setUp(self, _queue_write, _sleep):
        self._queue_write = _queue_write
        self._sleep = _sleep

        self.loop = asyncio.get_event_loop()
        self.bot = HordeWatchBot()

        self.loop.run_until_complete(self.bot.work(self.data))

    def test_msg_queued(self):
        """queue message for write notifying horde target"""
        expected = 'say "Calmconfused being targetted by spawned horde!"'
        self._queue_write.assert_called_once_with(expected)

    def test_no_sleep(self):
        """this bot doesn't sleep"""
        self._sleep.assert_not_called()
