import asyncio
from unittest import TestCase
from unittest.mock import patch, Mock

from testing import AsyncMock

from botman.bots import PlayerWatchBot
from botman.commands import LIST_PLAYERS


class ReTest(TestCase):

    def setUp(self):
        self.re = PlayerWatchBot.re_player_count

    def test_matches(self):
        self.assertTrue(
            self.re.search(b'Total of 1 in the game')
        )

    def test_group(self):
        match = self.re.match(b'Total of 1 in the game')
        self.assertEquals(1, int(match.group('count')))

class PlayerWatchBotTests(TestCase):

    def setUp(self):
        self.loop = asyncio.get_event_loop()

        self.bot = PlayerWatchBot()
        self.bot.on_active_players = Mock()
        self.bot.on_no_active_players = Mock()

        self.data_player = [
            b'garbage text',
            b'Total of 1 in the game',
            b'garbage text',
        ]
        self.data_no_player = [
            b'garbage text',
            b'Total of 0 in the game',
            b'garbage text',
        ]

    def test_callbacks_asserted(self):
        """work should fail if no callbacks set"""
        bot = PlayerWatchBot()
        bot.on_active_players = Mock()

        with self.assertRaises(AssertionError):
            self.loop.run_until_complete(bot.work([]))

        bot = PlayerWatchBot()
        bot.on_no_active_players = Mock()

        with self.assertRaises(AssertionError):
            self.loop.run_until_complete(bot.work([]))

    def test_initial_non_parsing(self):
        """not in parsing state when first setup"""
        self.assertFalse(self.bot.parsing)

    @patch('botman.bots.PlayerWatchBot.queue_write', new_callable=AsyncMock)
    def test_send_command(self, _queue_write):
        """
        if not in the parsing state issue server command to get player listing, should
        be in the parsing state afterwards
        """
        self.loop.run_until_complete(self.bot.work([]))
        _queue_write.assert_called_once_with(LIST_PLAYERS)

    @patch('botman.bots.PlayerWatchBot.queue_write', new_callable=AsyncMock)
    def test_parsing_after_command(self, _queue_write):
        """after command sent should be in parsing state"""
        self.loop.run_until_complete(self.bot.work([]))
        self.assertTrue(self.bot.parsing)

    @patch('botman.bots.PlayerWatchBot.queue_write', new_callable=AsyncMock)
    def test_only_send_once(self, _queue_write):
        """if in parsing state do not send command"""
        self.loop.run_until_complete(self.bot.work([]))
        self.loop.run_until_complete(self.bot.work([]))
        _queue_write.assert_called_once_with(LIST_PLAYERS)

    @patch('botman.bots.PlayerWatchBot.queue_write', new_callable=AsyncMock)
    @patch('botman.bots.PlayerWatchBot.sleep')
    def test_sleep_after(self, _sleep, _queue_write):
        """once target text is parsed sleep for 60 seconds"""
        self.bot.parsing = True
        self.loop.run_until_complete(self.bot.work(self.data_player))
        _queue_write.assert_not_called()
        _sleep.assert_called_once_with(60)

    @patch('botman.bots.PlayerWatchBot.sleep')
    def test_on_active_players(self, _sleep):
        """
        if there are active players invoke the appropriate callback passing
        the actual number of players
        """
        self.bot.on_active_players = Mock()
        self.bot.parsing = True
        self.loop.run_until_complete(self.bot.work(self.data_player))

        self.bot.on_active_players.assert_called_once_with(1)
        self.bot.on_no_active_players.assert_not_called()

    @patch('botman.bots.PlayerWatchBot.sleep')
    def test_on_no_active_players(self, _sleep):
        """if there are no active players invoke the appropriate callback"""
        self.bot.on_no_active_players = Mock()
        self.bot.parsing = True
        self.loop.run_until_complete(self.bot.work(self.data_no_player))

        self.bot.on_no_active_players.assert_called_once_with()
        self.bot.on_active_players.assert_not_called()

    @patch('botman.bots.PlayerWatchBot.sleep')
    def test_no_longer_parsing(self, _sleep):
        """once parsing is complete state should reflect"""
        self.bot.parsing = True
        self.loop.run_until_complete(self.bot.work(self.data_player))

        self.assertFalse(self.bot.parsing, 'bot should not longer be parsing!')
