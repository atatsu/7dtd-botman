from asyncio import coroutine
from unittest.mock import Mock


def mock_coroutine(return_value):
    @coroutine
    def mock_coro(*args, **kwargs):
        return return_value

    return Mock(wraps=mock_coro)


class AsyncMock(Mock):

    def __call__(self, *args, **kwargs):
        sup = super()
        async def coro():
            return sup.__call__(*args, **kwargs)
        return coro()

    def __await__(self):
        return self().__await__()
