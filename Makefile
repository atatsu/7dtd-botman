.PHONY: unit unit-debug unit-continous purge-pyc

unit:
	nosetests --tests tests --verbosity 3;

unit-debug:
	nosetests --tests tests --verbosity 3 -s;

unit-continuous:
	sniffer;

purge-pyc:
	find . -name "*.pyc" -delete;
