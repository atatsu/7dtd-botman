import re
from logging import getLogger
from typing import Callable

from ..commands import LIST_PLAYERS
from . import InternalBot

LOG = getLogger(__name__)


class PlayerWatchBot(InternalBot):
    """
    Simply keeps track of the number of players currently on the server
    so that the server can make decisions based on that number.
    """

    re_player_count = re.compile(b'^Total of (?P<count>[0-9]+) in the game\s?$')

    @property
    def on_no_active_players(self) -> Callable[[], None]:
        return self._on_no_active_players

    @on_no_active_players.setter
    def on_no_active_players(self, cb: Callable[[], None]):
        self._on_no_active_players = cb

    @property
    def on_active_players(self) -> Callable[[int], None]:
        return self._on_active_players

    @on_active_players.setter
    def on_active_players(self, cb: Callable[[int], None]):
        self._on_active_players = cb

    def __init__(self):
        super().__init__()
        self.parsing = False
        self._on_no_active_players = None
        self._on_active_players = None

    async def work(self, data):
        assert self.on_no_active_players is not None
        assert self.on_active_players is not None

        if not self.parsing:
            LOG.debug('Queueing command %r', LIST_PLAYERS)
            await self.queue_write(LIST_PLAYERS)
            self.parsing = True
            return

        for line in data:
            self.LOG_PARSE.debug('Parsing: %r', line.decode())
            match = self.re_player_count.match(line)
            if not match: continue

            player_count = int(match.group('count'))

            if player_count > 0:
                self.on_active_players(player_count)
            else:
                self.on_no_active_players()

            self.sleep(60)
            self.parsing = False
            break
