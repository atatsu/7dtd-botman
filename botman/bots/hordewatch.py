from logging import getLogger
import re

from . import Bot

LOG = getLogger(__name__)


class HordeWatchBot(Bot):
    """
    Announces spawned hordes and who they're targetting.
    """
    re_horde_spawn = re.compile((b'Spawning wandering horde moving towards player '
                                 b"'\[type=EntityPlayer, name=(?P<player_name>.+), "
                                 b"id=[0-9]+\]'"))

    def __init__(self):
        super().__init__()
        self.parsing = False

    async def work(self, data):
        for line in data:
            self.LOG_PARSE.debug('Parsing: %r', line)
            match = self.re_horde_spawn.search(line)

            if not match:
                continue

            player = match.group('player_name').decode()
            cmd = 'say "{} being targetted by spawned horde!"'.format(player)
            LOG.info(cmd)
            await self.queue_write(cmd)
