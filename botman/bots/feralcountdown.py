import re
from logging import getLogger
from datetime import datetime

from . import Bot
from ..commands import GET_TIME

LOG = getLogger(__name__)


class FeralCountdownBot(Bot):
    """
    Periodically announces how much time remains until the next
    feral horde. Notification frequency picks up the day before
    the horde and even more so on the day of the horde.
    """
    re_day_time = re.compile(
        b'^Day (?P<day>[0-9]+), (?P<hour>[0-9]{2}):(?P<minute>[0-9]{2})\s?$'
    )

    def __init__(self):
        super().__init__()
        self.parsing = False

    async def work(self, data):
        if not self.parsing:
            LOG.debug('Queueing command %r', GET_TIME)
            await self.queue_write(GET_TIME)
            self.parsing = True

        day = hour = minute = None
        for line in data:
            self.LOG_PARSE.debug('Parsing: %r', line)
            match = self.re_day_time.search(line)

            if not match:
                continue

            day, hour, minute = (
                int(match.group('day')),
                match.group('hour').decode(),
                match.group('minute').decode()
            )
            LOG.debug('Day: %s, Time: %s:%s', day, hour, minute)
            break

        if not day:
            # no match yet so wait til next round
            return

        self.parsing = False

        (feral_day,
         feral_spawn_time,
         current_time,
         days_remaining) = self._get_metrics(day, hour, minute)

        # check if the feral horde has already spawned
        if days_remaining == 0 and current_time >= feral_spawn_time:
            return self._already_spawned()

        # if we have at least a day til the feral horde spawns we'll update in
        # 15 minute intervals
        if days_remaining > 1:
            return await self._more_than_day(days_remaining, feral_day)

        # if feral horde will spawn in less than a day (roughly), update in 10 minute intervals
        if days_remaining == 1:
            return await self._less_than_day(feral_day)

        # we're on the day of the horde, update in 5 minute intervals
        await self._day_of(feral_spawn_time, current_time, feral_day)

    def _get_metrics(self, day, hour, minute):
        feral_day = None
        if day % 7 != 0:
            # not a feral day, so how much longer til the next one?
            for x in range(day, day+7):
                if x % 7 == 0:
                    feral_day = x
                    break
        else:
            feral_day = day

        feral_spawn_time = datetime.strptime('22:00', '%H:%M')
        current_time = datetime.strptime('{}:{}'.format(hour, minute), '%H:%M')
        days_remaining = feral_day - day

        return feral_day, feral_spawn_time, current_time, days_remaining


    def _already_spawned(self):
        LOG.debug('Feral horde has already spawned, going to sleep')
        self.sleep(15*60)

    async def _more_than_day(self, days_remaining, feral_day):
        cmd = 'say "Feral horde will spawn in {} days ({} 22:00)."'.format(
            days_remaining, feral_day
        )
        LOG.info(cmd)
        await self.queue_write(cmd)
        self.sleep(15*60)

    async def _less_than_day(self, feral_day):
        cmd = 'say "Feral horde will spawn in 1 day ({} 22:00)!"'.format(feral_day)
        LOG.info(cmd)
        await self.queue_write(cmd)
        self.sleep(10*60)

    async def _day_of(self, feral_spawn_time, current_time, feral_day):
        diff = feral_spawn_time - current_time
        hours_remaining = int(diff.seconds / 60 / 60)
        minutes_remaining = int(diff.seconds / 60 % 60)
        cmd = ('say "Feral horde will spawn in 0 days, {} hours, '
               'and {} minutes ({} 22:00)!"').format(
                   hours_remaining, minutes_remaining, feral_day
               )
        LOG.info(cmd)
        await self.queue_write(cmd)
        self.sleep(5*60)
