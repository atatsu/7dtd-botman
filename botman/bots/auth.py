from asyncio import Future
from logging import getLogger
from typing import Callable

from . import InternalBot
from ..exceptions import AuthenticationError

LOG = getLogger(__name__)


class AuthBot(InternalBot):
    """
    Internal bot and as such it behaves a little differently
    than the standard interface.
    """

    def __init__(self, password: str, future: Future):
        super().__init__()

        self.future = future
        self.password = password
        self.authenticating = False

    async def work(self, data):

        for line in data:
            self.LOG_PARSE.debug(line.decode())
            if not self.authenticating and b'Please enter password' in line:
                self.authenticating = True
                LOG.debug('Queueing password for write')
                await self.queue_write(self.password)
                return

            if self.authenticating and b'Password incorrect' in line:
                LOG.debug('Invalid password')
                await self.deactivate()
                self.future.set_exception(AuthenticationError)
                return

            if self.authenticating and b'Logon successful' in line:
                LOG.debug('Successfully authenticated')
                await self.deactivate()
                self.future.set_result(None)
                return
