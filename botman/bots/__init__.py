from abc import ABCMeta, abstractmethod
import asyncio
from asyncio import BaseEventLoop
from contextlib import suppress
from logging import getLogger
from typing import List, Union

LOG = getLogger(__name__)


class BotMeta(ABCMeta):

    def __str__(cls):
        return cls.__name__


class Bot(metaclass=BotMeta):

    # standard props
    @property
    def LOG_PARSE(self):
        if self._LOG_PARSE is None:
            self._LOG_PARSE = getLogger('{}.parse.{}'.format(
                __name__,
                self.__class__.__module__.split('.')[-1]
            ))
        return self._LOG_PARSE

    @property
    def is_active(self):
        return self._active

    @property
    def is_awake(self):
        return self._awake

    # props set by BotManager
    @property
    def loop(self):
        if self._loop is None:
            self._loop = asyncio.get_event_loop()
        return self._loop

    @loop.setter
    def loop(self, loop: BaseEventLoop):
        self._loop = loop

    @property
    def writer_put(self):
        assert self._writer_put is not None
        return self._writer_put

    @writer_put.setter
    def writer_put(self, writer_put):
        self._writer_put = writer_put

    def __init__(self):
        self._LOG_PARSE = None
        self._active = True
        self._awake = True
        self._loop = None
        self._writer_put = None

        self.task = None

    async def deactivate(self):
        self._active = False
        if self.task is None:
            return

        self.task.cancel()
        with suppress(asyncio.CancelledError):
            await self.task

    async def queue_write(self, msg: str) -> None:
        await self._writer_put(msg)
        LOG.debug('Queued %r for writing', msg)

    def sleep(self, seconds: Union[int, float]):
        self._awake = False

        if self.task is not None:
            self.task.cancel()
            asyncio.ensure_future(self._handle_cancel(self.task), loop=self.loop)

        self.task = asyncio.ensure_future(asyncio.sleep(seconds, loop=self.loop), loop=self.loop)
        self.task.add_done_callback(self._wake_up)

        LOG.debug('%s went to sleep (%ss)', self, seconds)
        return self.task

    @abstractmethod
    async def work(self, data: List[bytes]):
        pass

    async def _handle_cancel(self, task):
        with suppress(asyncio.CancelledError):
            await task

    def _wake_up(self, future=None):
        self._awake = True
        self.task = None
        LOG.debug('%s woke up', self)

    def __str__(self):
        return self.__class__.__name__


class InternalBot(Bot):
    """
    Designates a bot as internal which likely means it has non-standard
    behavior and does not adhere to the public bot interface.
    """

from .auth import AuthBot
from .playerwatch import PlayerWatchBot
from .feralcountdown import FeralCountdownBot
from .hordewatch import HordeWatchBot
