class BotMonException(Exception): pass

class AuthenticationError(BotMonException): pass
