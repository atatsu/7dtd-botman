from abc import ABCMeta, abstractmethod
import asyncio
from asyncio import BaseEventLoop, Queue, Task, Future
from contextlib import suppress
from functools import partial
from logging import getLogger
from telnetlib import Telnet
from typing import List, Callable, Union, Tuple

from .bots import Bot, AuthBot, PlayerWatchBot
from .exceptions import AuthenticationError

LOG = getLogger(__name__)
LOG_SERVER = getLogger('server')


class Periodic(metaclass=ABCMeta):

    LOG = None

    start_message = None
    stop_message = None

    def __init__(self, loop: BaseEventLoop=None):
        super().__init__()

        self._loop = loop or asyncio.get_event_loop()
        self._running = False
        self._shutting_down = False
        self._task = None
        self._cleanup_ran = False

        self.sleep_time = 1

    async def cleanup(self):
        """
        Override to perform cleanup tasks when `stop` is called.
        """

    def shutdown(self):
        self._shutting_down = True

    async def start(self, sleep_time: Union[int, float]=1) -> None:
        if self._running:
            return

        if self.start_message and self.LOG:
            self.LOG.debug(self.start_message)

        self.sleep_time = sleep_time

        self._running = True
        self._task = asyncio.ensure_future(self._run(), loop=self._loop)
        return self._task

    async def stop(self) -> None:
        if not self._running:
            return

        if self.stop_message and self.LOG:
            self.LOG.debug(self.stop_message)

        if not self._cleanup_ran:
            await self.cleanup()
            self._cleanup_ran = True

        self._running = False
        self._task.cancel()

        with suppress(asyncio.CancelledError):
            await self._task

    @abstractmethod
    async def work(self): pass

    async def _run(self) -> None:
        while not self._shutting_down:
            await asyncio.sleep(self.sleep_time)
            await self.work()

        await self.cleanup()
        self._cleanup_ran = True
        await self.stop()


class QueueManager:
    """
    Simple base class that adds a solitary queue.
    """

    @property
    def has_data(self) -> bool:
        return not self._data_q.empty()

    @property
    def size_of(self) -> int:
        """
        Returns the size of the current data queue.
        """
        return self._data_q.qsize()

    @property
    def data_q(self) -> Queue:
        return self._data_q

    def __init__(self, maxsize: int=100, loop: BaseEventLoop=None):
        self._loop = loop or asyncio.get_event_loop()
        self._data_q = Queue(maxsize=maxsize, loop=self._loop)

        super().__init__(loop=self._loop)


class ReadCycleManager(QueueManager, Periodic):
    """
    Collect data from the telnet connection. Any data that comes in
    is queued up for retrieval and processing by the bot cycle.
    """

    LOG = getLogger('{}.reader'.format(__name__))

    start_message = 'Starting read cycle'
    stop_message = 'Stopping read cycle'

    @property
    def storage_mode(self) -> bool:
        """
        Instructs the ReadCycleManager whether or not it should hold on
        (queue) to received data.
        """
        return self._storage_mode

    @storage_mode.setter
    def storage_mode(self, storage_mode: bool):
        if storage_mode:
            self.LOG.debug('Storage mode')
        else:
            self.LOG.debug('Non-storage mode')
        self._storage_mode = storage_mode

    def __init__(self, telnet: Telnet, loop: BaseEventLoop=None):
        self._storage_mode = True
        self._telnet = telnet
        self._loop = loop or asyncio.get_event_loop()

        self._buffer = b''

        super().__init__(loop=self._loop)

    async def all_data(self) -> Tuple[bytes]:
        """
        Grab a list of all data currently in the queue.
        """
        data = []
        for x in range(self.data_q.qsize()):
            data.append(await self.data_q.get())

        return tuple(data)

    async def work(self):
        data = self._buffer + self._telnet.read_very_eager()

        if not self._storage_mode:
            self.LOG.debug('non-storage mode: %s', data)
            self._buffer = b''
            return

        if data:
            self.LOG.debug(data)

        abort = False
        if not data:
            self.LOG.debug('No data')
            abort = True
        elif data == b'\r\n':
            # don't care about blank lines
            self.LOG.debug('Discarding blank line')
            abort = True
        elif b'\r\n' not in data:
            # incomplete read
            self.LOG.debug('Incomplete read, buffering for next round')
            self._buffer += data
            abort = True

        if abort:
            return

        parts = data.split(b'\r\n')
        # remove empty splits
        while b'' in parts:
            parts.remove(b'')
        if data[-2:] != b'\r\n':
            # incomplete line at the end so save it for next read operation
            self._buffer = parts.pop()

        for line in parts:
            await self.data_q.put(line)
            self.LOG.debug('Added %r to data queue', line)


class WriteCycleManager(QueueManager, Periodic):
    """
    Manages a queue of data that gets written to the telnet connection.
    """

    LOG = getLogger('{}.writer'.format(__name__))

    start_message = 'Starting write cycle'
    stop_message = 'Stopping write cycle'

    def __init__(self, telnet: Telnet, loop: BaseEventLoop=None):
        self._telnet = telnet
        self._loop = loop or asyncio.get_event_loop()

        super().__init__(loop=self._loop)

    async def work(self):
        for x in range(self.size_of):
            line = await self.data_q.get()
            self._telnet.write(line.encode('ascii') + b'\n')
            WriteCycleManager.LOG.debug('Wrote %r', line)


class SleepCycleManager(Periodic):
    """
    Polls the active bot queue for any bots that have gone to sleep and if
    any are found they are moved to the sleeping bot queue. Also polls the sleeping
    bot queue for any bots that have awoken and if any are found they are moved
    to the active bot queue.
    """
    LOG = getLogger('{}.sleepcycle'.format(__name__))

    start_message = 'Starting sleep cycle manager'
    stop_message = 'Stopping sleep cycle manager'

    @property
    def asleep_count(self) -> int:
        return self._asleep_q.qsize()

    @property
    def awake_count(self) -> int:
        return self._awake_q.qsize()

    @property
    def phase_shift_q(self) -> Queue:
        return self._phase_shift_q

    @property
    def phase_shift_excludes(self) -> Tuple[Bot]:
        return self._phase_shift_excludes

    def __init__(self, asleep_queue: Queue, awake_queue: Queue, loop: BaseEventLoop = None):
        self._loop = loop or asyncio.get_event_loop()
        self._asleep_q = asleep_queue
        self._awake_q = awake_queue

        self._phase_shift_q = Queue(maxsize=100, loop=self._loop)
        self._phase_shift = False
        self._phase_shift_excludes = ()

        super().__init__(loop=self._loop)

    async def cleanup(self):
        for x in range(self._phase_shift_q.qsize()):
            bot = await self._phase_shift_q.get()
            # we're shutting down at this point so no need to requeue
            await bot.deactivate()

    def end_phase_shift(self):
        self._phase_shift = False

    def phase_shift(self, *excludes: Tuple[Bot]) -> None:
        """
        Until told to stop all active (sleeping or awake) bots will be phase shifted
        out of the normal bot cycle.

        Args:
            excludes (tuple): Instances of any bot classes passed here will be
                excluded from the phase shift.
        """
        if not self._phase_shift:
            self._phase_shift_excludes = excludes
        self._phase_shift = True

    async def work(self):

        if self._phase_shift and self._phase_shift_q.empty():
            # so that we're not double processing bots get a snapshot of q sizes before we start
            asleep_count = self.asleep_count
            awake_count = self.awake_count

            self.LOG.info(
                'Phase shifting bots (excludes: %s)',
                tuple([str(bot) for bot in self.phase_shift_excludes])
            )

            await self._phase_shift_bots(asleep_count, self._asleep_q)
            await self._phase_shift_bots(awake_count, self._awake_q)

        # phase shift isn't active at this point, need to check if there are any
        # phase shifted bots that need to be returned to their respective qs
        if not self._phase_shift and not self._phase_shift_q.empty():
            shifted_count = self._phase_shift_q.qsize()
            self.LOG.info('Returning phase shifted bots to their respective queues')
            for x in range(shifted_count):
                bot = await self._phase_shift_q.get()
                if bot.is_awake:
                    await self._awake_q.put(bot)
                    self.LOG.debug('%s bot restored to awake queue', bot)
                    continue

                await self._asleep_q.put(bot)
                self.LOG.debug('%s bot restored to asleep queue', bot)

        self.LOG.debug('Checking %s sleeping bots for any that awoke', self.asleep_count)
        for x in range(self.asleep_count):
            bot = await self._asleep_q.get()

            if not bot.is_active:
                self.LOG.info('%s deactive, removing from cycle', bot)
                # lack of a requeue effectively removes it
                continue

            if bot.is_awake:
                self.LOG.debug('%s bot woke up, moving to awake queue', bot)
                await self._awake_q.put(bot)
                continue

            await self._asleep_q.put(bot)

        self.LOG.debug('Checking %s waking bots for any that fell asleep', self.awake_count)
        for x in range(self.awake_count):
            bot = await self._awake_q.get()

            if not bot.is_active:
                self.LOG.info('%s deactive, removing from cycle', bot)
                # lack of a requeue effectively removes it
                continue

            if not bot.is_awake:
                self.LOG.debug('%s bot fell asleep, moving to asleep queue', bot)
                await self._asleep_q.put(bot)
                continue

            await self._awake_q.put(bot)

    async def _phase_shift_bots(self, count: int, q: Queue):
        for x in range(count):
            bot = await q.get()
            if isinstance(bot, self._phase_shift_excludes):
                self.LOG.debug('%s excluded from phase shift', bot)
                # don't forget to requeue!
                await q.put(bot)
                continue
            await self._phase_shift_q.put(bot)
            self.LOG.debug('%s bot phase shifted', bot)


class BotManager(Periodic):

    LOG = getLogger('{}.botmanager'.format(__name__))

    start_message = 'Starting bot cycle'
    stop_message = 'Stopping bot cycle'

    @property
    def asleep_count(self):
        return self._asleep_bots.qsize()

    @property
    def awake_count(self):
        return self._awake_bots.qsize()

    @property
    def loop(self) -> BaseEventLoop:
        return self._loop

    def __init__(self, host, port, password, loop=None):
        self._host = host
        self._port = port
        self._password = password

        self._loop = loop or asyncio.get_event_loop()

        self._registered_bots = []
        self._awake_bots = Queue(loop=self._loop)
        self._asleep_bots = Queue(loop=self._loop)

        self._reader = None
        self._writer = None
        self._sleeper = None
        self._telnet = None

        # cycle flags
        self._authenticated = False

        super().__init__(loop=self._loop)

    async def cleanup(self):
        self._telnet.close()

        await self._reader.stop()
        await self._writer.stop()
        await self._sleeper.stop()

        self.LOG.info('Deactivating all bots')
        bots = []
        for bot in range(self.asleep_count):
            bots.append(await self._asleep_bots.get())
        for bot in range(self.awake_count):
            bots.append(await self._awake_bots.get())

        for bot in bots:
            await bot.deactivate()

    def register_bots(self, *bots):
        for bot in bots:
            assert isinstance(bot, Bot), 'Must be a Bot instance'
            self._registered_bots.append(bot)
            LOG.debug('Registered %s', bot)

    async def start(self):
        """
        Establishes a telnet connection with the server and then
        starts cycling through bots.
        """
        LOG.info('Attempting connection to %s:%s', self._host, self._port)
        try:
            self._telnet = Telnet(host=self._host, port=self._port)
            LOG.info('Connection successful')
        except ConnectionRefusedError:
            LOG.error('Connection failed, verify host/port')
            raise

        self._reader = ReadCycleManager(self._telnet, loop=self._loop)
        self._writer = WriteCycleManager(self._telnet, loop=self._loop)
        self._sleeper = SleepCycleManager(
            asleep_queue=self._asleep_bots, awake_queue=self._awake_bots, loop=self._loop
        )

        # start the bot cycle off with only AuthBot, if it is able
        # to successfully authenticate the rest of the bots will
        # be added to the mix
        self._add_auth_bot()

        await self._reader.start()
        await self._writer.start()
        await self._sleeper.start()
        await super().start()

    async def stop(self):
        if not self._shutting_down:
            await super().stop()
        if self._loop.is_running():
            self._loop.stop()

    async def work(self):
        """
        Cycles through every registered bot. For each bot will
        check whether bot is awake or asleep. If asleep will simply
        requeue bot. If awake will pass it any data that has been
        received lately and instruct it to work. Afterwards if bot
        is still active the bot will be requeued. Inactive bots
        are removed from the internal bot list.
        """
        if not self.awake_count and self._reader.storage_mode:
            # if there are no active bots no point in holding on to any
            # data from the server
            self._reader.storage_mode = False
        elif self.awake_count and not self._reader.storage_mode:
            # conversely, if there are active bots but the reader isn't storing
            # data instruct it to do so
            self._reader.storage_mode = True

        if not self.awake_count:
            # don't bother cycling if there aren't any active bots
            LOG.debug('No active bots or all bots sleeping')
            return

        # collect all recently received data
        data = await self._reader.all_data()

        LOG.debug('Cycling through %s bots', self.awake_count)

        futures = []
        for x in range(self.awake_count):
            bot = await self._awake_bots.get()

            # schedule a task instructing each bot to work with
            # a copy of the most recently read data
            LOG.debug('Instructing %s to work', bot)
            future = asyncio.ensure_future(bot.work(data[:]))
            log_msg = '{} work cycle complete'.format(bot)
            future.add_done_callback(
                partial(self._debug_log_callback, log_msg)
            )
            futures.append(future)

            # put our bot back on the queue
            await self._awake_bots.put(bot)

        if futures:
            LOG.debug('Waiting for all bot cycles to finish')
            await asyncio.wait(futures, loop=self._loop)
            return

    def _add_auth_bot(self):
        auth_future = Future()
        auth_future.add_done_callback(self._on_auth_done)
        authbot = AuthBot(self._password, auth_future)
        authbot.loop = self._loop
        authbot.writer_put = self._writer.data_q.put
        self._awake_bots.put_nowait(authbot)
        self

    def _debug_log_callback(self, msg, future):
        self.LOG.debug(msg)

    # handlers
    def _on_auth_done(self, future=None):
        if future.exception() is not None:
            LOG.error('Authentication failed, shutting down')
            self.shutdown()
            return

        LOG.info('Successfully authenticated')
        self._authenticated = True

        # get PlayerWatchBot into the rotation
        playerwatch = PlayerWatchBot()
        playerwatch.on_no_active_players = self._on_no_active_players
        playerwatch.on_active_players = self._on_active_players
        self._registered_bots.insert(0, playerwatch)

        LOG.debug('Adding all registered bots to active bot queue')
        for bot in self._registered_bots:
            bot.writer_put = self._writer.data_q.put
            bot.loop = self._loop
            self.LOG.debug('Adding %s to active bot queue', bot)
            asyncio.ensure_future(self._awake_bots.put(bot))

    def _on_no_active_players(self):
        self._sleeper.phase_shift(PlayerWatchBot)

    def _on_active_players(self, count):
        self._sleeper.end_phase_shift()
